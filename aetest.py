from __future__ import print_function
import pycurl, sys, time, re

try:
    from io import BytesIO
except ImportError:
    from StringIO import StringIO as BytesIO

USERNAME = "testuser"
PASSWORD = "54321password12345"
LOGIN_DATA = "csrfmiddlewaretoken={t}&username={u}&password={p}"
BASE_URL = "http://authenticationtest.herokuapp.com/"
INTERVAL = 30

class ActualTester(object):
    def __init__(self, interval=INTERVAL):
        # login details
        self.username = USERNAME
        self.password = PASSWORD
        self.url = BASE_URL

        # interval between tests
        self.interval = interval

        # cookie dictionary
        self.cookies = {"_next_": "root"}

        # handle to pycurl.Curl() for testing RTT and goodput
        self._test_handle = None

        # data
        self.total_requests = 0
        self.failed_requests = 0
        self.rtt = []
        self.goodput = []

        # log
        self.log = ""

    @property
    def cookie_header(self):
        return "Cookie: " + "; ".join(k+"="+v for (k, v) in self.cookies.items())

    @property
    def login_data(self):
        token = self.cookies['csrftoken']
        encoded_data = "csrfmiddlewaretoken={t}&username={u}&password={p}"
        return encoded_data.format(t=token, u=self.username, p=self.password)

    @property
    def test_handle(self):
        if self._test_handle is None:
            self._test_handle = pycurl.Curl()
            self._test_handle.setopt(pycurl.HTTPHEADER, [self.cookie_header,])
            self._test_handle.setopt(pycurl.URL, self.url)
            self._test_handle.setopt(pycurl.WRITEFUNCTION, lambda x: None)
        return self._test_handle

    def cookie_cutter(self, buf):
        """Detects Set-Cookie headers and updates cookie dictionary
        buf:
            HTTP response header buffer
        Usage:
            > c = pycurl.Curl()
            > c.setopts(c.URL, 'http://example.com')
            > c.setopts(c.HEADERFUNCTION, cookie_cutter)
            > c.perform()
        """
        pattern = r'(?<=Set-Cookie: )[a-zA-Z0-9-]+=[a-zA-Z0-9-]+'
        buffer = BytesIO()
        buffer.write(buf)
        line = buffer.getvalue().decode("iso-8859-1")
        self.log += line
        matches = re.findall(pattern, line)
        found_cookies = dict([x.split("=") for x in matches])
        self.cookies.update(found_cookies)

    def authenticate(self):
        print("Fetching login csrftoken... ", end="")
        sys.stdout.flush()
        c = pycurl.Curl()
        c.setopt(c.URL, BASE_URL + "login/")
        c.setopt(c.HEADER, True)
        c.setopt(c.WRITEFUNCTION, self.cookie_cutter)
        c.perform()
        print(self.cookies["csrftoken"])

        print("Logging in (with _next_ = root)...")
        c.setopt(c.URL, BASE_URL + "login/ajax/")
        c.setopt(c.HTTPHEADER, [self.cookie_header,])
        c.setopt(c.POSTFIELDS, self.login_data)
        c.perform()
        print("New token: ", self.cookies["csrftoken"])
        print("Session id: ", self.cookies["sessionid"])

    def test(self):
        print()
        print("Sending request to test server every %i seconds" % self.interval)
        print("Press Ctrl-C to print summary and exit")
        print()
        while True:
            print("Fetching... ", end="")
            sys.stdout.flush()
            try:
                self.test_handle.perform()
            except KeyboardInterrupt:
                self.print_and_exit()
            except pycurl.error as e:
                print("Failed to connect. Retrying in %i seconds." % self.interval)
                self.failed_requests += 1
            self.total_requests += 1
            rtt = self.test_handle.getinfo(pycurl.TOTAL_TIME)
            size = self.test_handle.getinfo(pycurl.SIZE_DOWNLOAD)
            goodput = 8*self.test_handle.getinfo(pycurl.SPEED_DOWNLOAD)
            print("Got %i bytes in %.2f seconds" % (size, rtt))
            self.rtt.append(rtt)
            self.goodput.append(goodput)
            try:
                time.sleep(self.interval)
            except KeyboardInterrupt:
                self.print_and_exit()
    def print_and_exit(self):
        if len(self.rtt) > 0:
            mean_RTT = sum(self.rtt)/len(self.rtt)
            mean_goodput = sum(self.goodput)/len(self.goodput)

            print()
            print("Mean RTT:  %.4f seconds" % mean_RTT)
            print("Mean goodput:  %.2f bits/second" % mean_goodput)
            print("%i out of %i requests failed" % (self.failed_requests, self.total_requests))
        sys.exit(0)
        print("Done")

if __name__ == "__main__":
    tester = ActualTester()
    tester.authenticate()
    tester.test()
