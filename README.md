# AEtest

A python script using pycurl to authenticate with the Actual Experience test server (http://authenticationtest.herokuapp.com/), and take periodic measurements of goodput and RTT (round trip time).

### Supported versions and platforms

AEtest has been tested using Python 2.7 and 3.5 on MacOS 10.11.6.


### Dependencies

AEtest's only external dependency is pycurl

### Usage

> python aetest.py

The script will run until the user enters Ctrl-C, and then print summary statistics and exit.

### Round Trip Time

Round trip time was measured using pycurl.TOTAL_TIME.

### Goodput

Goodput was estimated using pycurl.SPEED_DOWNLOAD.

### Author

Joseph Harvey
